/*
Mangadex@Home
Copyright (c) 2020, MangaDex Network
This file is part of MangaDex@Home.

MangaDex@Home is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MangaDex@Home is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this MangaDex@Home.  If not, see <http://www.gnu.org/licenses/>.
 */
package mdnet.base.settings

import com.fasterxml.jackson.annotation.JsonUnwrapped
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.databind.annotation.JsonNaming
import dev.afanasev.sekret.Secret

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy::class)
class ClientSettings(
    val maxCacheSizeInMebibytes: Long = 20480,
    val webSettings: WebSettings? = null,
    val devSettings: DevSettings = DevSettings(isDev = false)
) {
    // FIXME: jackson doesn't work with data classes and JsonUnwrapped
    // fix this in 2.0 when we can break the settings file
    // and remove the `@JsonUnwrapped`
    @field:JsonUnwrapped
    lateinit var serverSettings: ServerSettings

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ClientSettings

        if (maxCacheSizeInMebibytes != other.maxCacheSizeInMebibytes) return false
        if (webSettings != other.webSettings) return false
        if (devSettings != other.devSettings) return false
        if (serverSettings != other.serverSettings) return false

        return true
    }

    override fun hashCode(): Int {
        var result = maxCacheSizeInMebibytes.hashCode()
        result = 31 * result + (webSettings?.hashCode() ?: 0)
        result = 31 * result + devSettings.hashCode()
        result = 31 * result + serverSettings.hashCode()
        return result
    }

    override fun toString(): String {
        return "ClientSettings(maxCacheSizeInMebibytes=$maxCacheSizeInMebibytes, webSettings=$webSettings, devSettings=$devSettings, serverSettings=$serverSettings)"
    }
}

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy::class)
data class ServerSettings(
    val maxMebibytesPerHour: Long = 0,
    val maxKilobitsPerSecond: Long = 0,
    val clientHostname: String = "0.0.0.0",
    val clientPort: Int = 443,
    val clientExternalPort: Int = 0,
    @field:Secret val clientSecret: String = "PASTE-YOUR-SECRET-HERE",
    val threads: Int = 4,
    val gracefulShutdownWaitSeconds: Int = 60
)

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy::class)
data class WebSettings(
    val uiHostname: String = "127.0.0.1",
    val uiPort: Int = 8080
)

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy::class)
data class DevSettings(
    val isDev: Boolean = false
)
